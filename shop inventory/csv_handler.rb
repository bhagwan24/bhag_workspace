module CsvHandler

  include InputHandler

  def list_products
     records = CSV.read("inventory.csv", headers: true, :col_sep => ",")
  end

  def destroy_product product_id
    updated_records = []
    CSV.foreach("inventory.csv", headers: true, header_converters: :symbol) do |row|
      if row[:product_id] != product_id
        updated_records << row
      end
    end
    updated_records
  end

  def update_products records
    CSV.open("inventory.csv",'wb', :write_headers => true, :headers => ["product_id", "name", "price", "stock_items", "company_name"]){|file|
      records.each do |row|
        file << row
      end
    }
  end

  def add_product product
    CSV.open("inventory.csv",'a+', skip_blanks: true){|row|
      row << [product.id, product.name, product.price, product.stock_items, product.company_name]
    }
  end

  def search_product prod_name
    searched_result = []
    CSV.foreach("inventory.csv", headers: true, header_converters: :symbol) do |row|
      if row[:name].include? prod_name
        searched_result << row
      end
    end
    searched_result
  end

  def get_product_details product_id
    products = []
    CSV.foreach("inventory.csv", headers: true, header_converters: :symbol) do |row|
      if row[:product_id] == product_id
        puts "Product Details:"
        puts "#{row}"
        name = user_input "Enter product name", "string", "Invalid input"
        price = user_input "Enter price", "numeric", "Invalid input"
        stock_items = user_input "Enter stock items", "numeric", "Invalid input"
        company_name = user_input "Enter company name", "string", "Invalid input"
        products << [row[:product_id], name, price, stock_items, company_name]
      else
        products << row
      end
    end
    products
  end

end