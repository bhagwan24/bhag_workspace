module InputHandler

  def genarate_product_id
    ([*('0'..'9'),*('A'..'Z')]).sample(5).shuffle.join
  end

  def is_numeric? x
    x.match(/[0-9]/) ? true : false
  end


  def get_product_id
    id = user_input "Enter product_id", "string", "Invalid input"
    {id: id}
  end

  def user_input msg, type, err_msg = nil, min = nil, max = nil
    puts msg
    data = gets.strip
    if is_valid? min, max, data
      if type == "numeric"
        return data.to_i
      else
        return data
      end
    else
      puts err_msg
      user_input msg, type, min, max
    end
  end

  def is_valid? min, max, data
    valid = true
    if !min.nil? && !max.nil?
      len = data.length
      valid = len > min && len < max
    end
    valid
  end

  def get_product_name
    name = user_input "Enter product name", "string", "Invalid input"
    {name: name}
  end

  def new_product
    id = genarate_product_id
    name = user_input "Enter product details\nName:", "string", "Invalid input", 3, 16
    price = user_input "Price:", "numeric",  "Invalid input"
    stock_items = user_input "Stock items:", "numeric",  "Invalid input"
    company_name = user_input "Company Name:", "string",  "Invalid input"
    {id: id, name: name, price: price, stock_items: stock_items, company_name: company_name}
  end

  def launch_options
    user_input "Please enter serial number for all the options provided for the user.\nSelect an option\n1.Shopkeeper\n2.Customer\n3.Exit", "numeric", "Invalid input"
  end

  def launch_customer_options
    user_input "Select action\n1.List products\n2.Search product\n3.Buy product\n4.Exit", "numeric", "Invalid input"
  end

  def launch_shopkeeper_options
    user_input "Select action\n1.Add product\n2.Remove product\n3.List products\n4.Search product\n5.Edit product\n6.Exit", "numeric", "Invalid input"
  end

  def new_order
    product_id = input.user_input "Enter product details:\nEnter product id", "string", "Invalid input"
    name = input.user_input "Enter product name:", "string", "Invalid input"
    credit_card_no = input.user_input "Enter credit card number", "numeric", "Invalid input"
    cvv = input.user_input "Enter your CVV", "numeric", "Invalid input"
    {product_id: product_id, name: name, credit_card_no: credit_card_no, cvv: cvv}
  end

end