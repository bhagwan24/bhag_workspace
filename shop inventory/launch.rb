require_relative 'vendor'
require_relative 'customer'
require_relative 'input_handler'

require 'csv'

class Launch

	include InputHandler

	def start
		option = launch_options
		if option == 1
			vendor = Vendor.new
			vendor.shopkeeper_actions
			start
		elsif option == 2
			customer = Customer.new
			customer.customer_actions
			start
		elsif option == 3
			exit(0)
		else
			puts "Please enter valid option.\n"
			start
		end
	end

end

launch = Launch.new
launch.start