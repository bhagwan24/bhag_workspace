require_relative 'order'
require_relative 'input_handler'
require_relative 'inventory_handler'

class Product

	include InputHandler
	include CsvHandler

	attr_accessor :id, :name, :price, :stock_items, :company_name

	def index
		list_products
	end

	def initialize(params = {})
		params.each do |k, v|
      self.instance_variable_set("@#{k}",v)
    end
	end

	def create
		product = self
		if !product.nil?
			add_product product
			"Product added successfully."
		else
			"Enter valid data."
		end

	end

	def edit
		products = get_product_details self.id
    if products.length > 0
      update_products products
      "Product updated successfully."
    else
      "Product is out of stock or not available."
    end
	end

	def destroy
		records = index
    if records.length > 0
      updated_records = destroy_product self.id
      update_products updated_records
      "Product deleted successfully."
    else
      "No products to perform action."
    end
	end

	def search
		searched_result = search_product self.name
    if searched_result.length > 0
      puts "product_id,name,price,stock_item,company_name"
      searched_result
    else
      "Result not found."
    end
	end

	def buy_product params
		records = index
		if records.length > 0
			order = Order.new(params)
			order.place_order
		else
			"No products to perform action."
		end
	end

end
