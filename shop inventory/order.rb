class Order

  attr_accessor :product_id, :name, :credit_card_no, :cvv

  def initialize(params = {})
    params.each do |k, v|
      self.instance_variable_set("@#{k}",v)
    end
  end

  def place_order
    order = self
     p order.inspect
    CSV.open("orders.csv",'a+', skip_blanks: true){|row|
      row << [order.product_id, order.name, order.credit_card_no, order.cvv]
    }
    #decriment_stock_item order.product_id
  end

  def decriment_stock_item product_id
    updated_records = []
    CSV.foreach("inventory.csv", headers: true, header_converters: :symbol) do |row|
      if row[:product_id] == product_id
        updated_stock_items = row[:stock_items].to_i - 1
        updated_records << [row[:product_id], row[:name], row[:price], updated_stock_items, row[:company_name]]
      else
        updated_records << row
      end
    end
    CSV.open("inventory.csv",'wb', :write_headers => true, :headers => ["product_id", "name", "price", "stock_items", "company_name"]){|file|
      updated_records.each do |row|
        file << row
      end
    }
  end

  def validate_order
    is_product_available = false
    message = "Something went wrong!"
    CSV.foreach("inventory.csv", headers: true, header_converters: :symbol) do |row|
      if row[:product_id] == self.product_id && row[:name] == self.name
        if row[:stock_items].to_i != 0
          is_product_available = true
        end
        break
      end
    end
    if is_product_available
      self.place_order
      message = "Order placed successfully."
    else
      message = "Product is not available or out of stock!"
    end
    message
  end

end