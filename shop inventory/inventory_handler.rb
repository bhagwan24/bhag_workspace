require_relative 'input_handler'
require_relative 'csv_handler'

class InventoryHandler

  include CsvHandler

  attr_accessor :id, :name, :price, :stock_items, :company_name

  def new(inventory = {})
    inventory.each do |k, v|
      self.instance_variable_set("@#{k}",v)
    end
  end

  def index
    list_products
  end

  def save
    inventory = self
    add_product inventory
    "Product added successfully.\n"
  end

  def delete
    records = index
    if records.length > 0
      updated_records = destroy_product self.id
      update_products updated_records
      "Product deleted successfully."
    else
      "No products to perform action."
    end
  end

  def search
    searched_result = search_product self
    if searched_result.length > 0
      puts "product_id,name,price,stock_item,company_name"
      searched_result
    else
      "Result not found."
    end
  end

  def edit
    products = get_product_details self.id
    if products.length > 0
      update_products products
    else
      "Product is out of stock or not available."
    end
  end

end