require_relative 'product'
require_relative 'input_handler'
require_relative 'order'

class Customer

	include InputHandler

	def customer_actions
		action = launch_customer_options
		#product = Product.new
		case action
			when 1
				product = Product.new
				message = product.index
			when 2
				params = get_product_name
        product = Product.new(params)
				message = product.search
			when 3
				params = new_order
				product = Product.new
				message = product.buy_product params
			when 4
				exit(0)
			else
				puts "Please enter valid option"
				customer_actions
		end
		puts message
		puts "----------------------------------------------"
		customer_actions
	end

end