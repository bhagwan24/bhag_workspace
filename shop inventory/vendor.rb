require_relative 'product'
require_relative 'input_handler'

class Vendor

  include InputHandler

  def shopkeeper_actions
    action = launch_shopkeeper_options
    case action
      when 1
        params = new_product
        product = Product.new(params)
        message = product.create
      when 2
        params = get_product_id
        product = Product.new(params)
        message = product.destroy
      when 3
        product = Product.new
        message = product.index
      when 4
        params = get_product_name
        product = Product.new(params)
        message = product.search
      when 5
        params = get_product_id
        product = Product.new(params)
        message = product.edit
      when 6
        exit(0)
      else
        message = "Please enter valid option.\n"
        shopkeeper_actions
    end
    puts message
    puts "----------------------------------------------------------"
    shopkeeper_actions
  end

  def method_name

  end

end