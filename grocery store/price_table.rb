module PriceTable

  PRICE_TABLE = {
    milk: {
      unit_price: 3.97,
      sale_price: {
        quantity: 2,
        price: 5
      }
    },
    bread: {
      unit_price: 2.17,
      sale_price: {
        quantity: 3,
        price: 6
      }
    },
    banana: {
      unit_price: 0.99
    },
    apple: {
      unit_price: 0.89
    }
  }

  def get_price_table
    price_table = {}
    CSV.foreach("price_table.csv", headers: true, header_converters: :symbol) do |row|
      unit_price = row[:unit_price].gsub("$", "").to_f
      if !row[:sale_price].nil?
        offer = row[:sale_price].split("for")
        quantity = offer[0].to_i
        price = offer[1].gsub("$", "").to_f
        price_table[:"#{row[:item].downcase}"] = {
          unit_price: unit_price,
          sale_price: {
            quantity: quantity,
            price: price
          }
        }
      else
        price_table[:"#{row[:item].downcase}"] = {
            unit_price: unit_price,
        }
      end
    end
    price_table
  end

end