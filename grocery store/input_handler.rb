module InputHandler

  def user_input msg
    puts msg
    data = gets.strip
  end

  def get_user_order
    order = user_input "Please enter all the items purchased separated by a comma"
    order_list = order.split(",")
  end

end