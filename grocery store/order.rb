class Order

  include InputHandler

  attr_accessor :items, :total_price, :actual_price

  def new
    self.items = get_user_order
    self.total_price = 0
    self.actual_price = 0
    self
  end

  def show
     current_order = self
     puts "Total price: $#{current_order.total_price.round(2)}\nYou saved: $#{(current_order.actual_price - current_order.total_price).round(2)}"
  end

end