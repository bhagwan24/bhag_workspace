require_relative 'input_handler'
require_relative 'price_table'
require_relative 'order'
require 'csv'

class PriceCalculator

  include PriceTable

  def new_order
    order = Order.new.new
    order_list = get_order_list order.items
    order_list.each do |k, quantity|
      item = PRICE_TABLE[:"#{k}"]
      if item_has_offer? item, quantity
        order.total_price += calculate_offer_price item, quantity
      else
        order.total_price += calculate_unit_price item, quantity
      end
      order.actual_price += calculate_unit_price item, quantity
    end
    order.show
  end

   def item_has_offer? item, quantity
    !item[:sale_price].nil? && quantity >= item[:sale_price][:quantity]
  end

  def calculate_offer_price item, quantity
    item[:sale_price][:price] + (quantity - item[:sale_price][:quantity]) * item[:unit_price]
  end

  def calculate_unit_price item, quantity
    quantity * item[:unit_price]
  end

  def get_order_list order_list
    order_list.uniq.map { |x| [x, order_list.count(x)] }.to_h
  end

end

PriceCalculator.new.new_order